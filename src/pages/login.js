import * as React from 'react';
// import "./Login.scss";

import {
    Box,
    Button,
    TextField
} from '@mui/material';

export default function Login() {
    const [email, setEmail] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [error, setError] = React.useState("");

    const checkEmail = () => {
        return email == "" ? false : email.includes('@ynov.com') ? false : true;
    }
    const checkPassword = () => {
        if (password == "") { return false; }
        else { return password.length < 8; }
    }

    const handleEmail = (event) => {
        setEmail(event.target.value);
    };

    const handlePassword = (event) => {
        setPassword(event.target.value);
    };

    function validateForm() {
        const ok = email.length > 0 && email.includes('@') ? email.split('@')[1].includes("ynov.com") : false;
        return ok && password.length > 7;
    }

    function handleSubmit() {
        
    }

    return (
        <div className="Login">
            <h2 className="title">Se connecter</h2>
            <p>{error}</p>
            <Box
                component="form"
                sx={{
                    '& .MuiTextField-root': { m: 1, width: '25ch' },
                }}
                noValidate
                autoComplete="off"
            >

                <div className="containerForm">
                    <TextField
                        error={checkEmail()}
                        required
                        id="outlined-required"
                        label="Adresse email"
                        helperText={checkEmail() ? "L'email doit contenir @ynov.com" : ""}
                        value={email}
                        onChange={handleEmail}
                    />

                    {/* Un peu dégueu :) */}
                    <br></br>

                    <TextField
                        error={checkPassword()}
                        required
                        id="outlined-password-input"
                        label="Mot de passe"
                        helperText={checkPassword() ? "Le mot de passe doit faire au moins 8 caractères" : ""}
                        type="password"
                        value={password}
                        onChange={handlePassword}
                    />

                </div>

                <div>
                    <Button disabled={!validateForm()} variant="contained" onClick={handleSubmit}>
                        Se connecter
                    </Button>
                </div>

            </Box>
        </div>
    );
}
