import * as React from 'react';
// import "./Signup.scss";

import {
    Box,
    Button,
    TextField,
    Alert
} from '@mui/material';

export default function Signup() {

    const [email, setEmail] = React.useState("");
    const [phone, setPhone] = React.useState("");
    const [name, setName] = React.useState("");
    const [surName, setSurName] = React.useState("");
    const [balance, setBalance] = React.useState(0);
    const [password, setPassword] = React.useState("");
    const [emailConfirm, setEmailConfirm] = React.useState("");
    const [message, setMessage] = React.useState("");

    const checkName = () => {
        return name == "" ? false : name.length > 2 ? false : true;
    }
    const checkSurName = () => {
        return surName == "" ? false : surName.length > 2 ? false : true;
    }
    const checkEmail = () => {
        return email == "" ? false : email.includes('@') ? false : true;
    }
    const checkConfirmEmail = () => {
        return emailConfirm == "" ? false : emailConfirm.includes('@') ? false : true;
    }
    const checkPhone = () => {
        return phone == "" ? false : phone.length < 10 ? true : false;
    }
    const checkPassword = () => {
        if (password == "") { return false; }
        else { return password.length < 8; }
    }

    const handleName = (event) => {
        setName(event.target.value);
    };
    const handleSurName = (event) => {
        setSurName(event.target.value);
    };
    const handleEmail = (event) => {
        setEmail(event.target.value);
    };
    const handlePassword = (event) => {
        setPassword(event.target.value);
    };
    const handleEmailConfirm = (event) => {
        setEmailConfirm(event.target.value);
    };
    const handlePhone = (event) => {
        setPhone(event.target.value);
    };

    function validateForm() {
        const ok = email.length > 0 && email.includes('@') ? email.includes('@') : false;
        return ok && email == emailConfirm && password.length > 7;
    }


    function handleSubmit() {
        //event.preventDefault();
        console.log("bite");
        // const param = { 'email': email, 'pseudo': 'toto2', 'password': password };
        // API.post('/auth/signup', param)
        //     .then(response => {
        //         console.log('resp', response);
        //         const param = { 'email': email, 'password': password };

        //         API.post('/auth/login', param)
        //             .then(response => {
        //                 document.cookie = 'token=' + response.data.accessToken;
        //                 document.cookie = 'logged=true';
        //                 history.push("/home");
        //             })
        //             .catch(error => {
        //                 console.log('error : ', error.message);
        //             }
        //         );

        //     })
        //     .catch(error => {
        //         console.log('error : ', error);
        //     })
    }

    return (
        <div className="Signup">
            <h2 className="title">Créer un compte</h2>

            <Box
                component="form"
                sx={{
                    '& .MuiTextField-root': { m: 1, width: '25ch' },
                }}
                noValidate
                autoComplete="off"
            >
                <div className="containerForm">

                    <TextField
                        error={checkName()}
                        required
                        id="outlined-required"
                        label="Prénom"
                        helperText={checkName() ? "Le prénom doit contenir au moins 2 caractères" : ""}
                        value={name}
                        onChange={handleName}
                    />

                    <TextField
                        error={checkSurName()}
                        required
                        id="outlined-required"
                        label="Nom de famille"
                        helperText={checkSurName() ? "Le nom de famille doit contenir au moins 2 caractères" : ""}
                        value={surName}
                        onChange={handleSurName}
                    />

                    <br></br>

                    <TextField
                        error={checkEmail()}
                        required
                        id="outlined-required"
                        label="Adresse email"
                        helperText={checkEmail() ? "L'email doit contenir @" : ""}
                        value={email}
                        onChange={handleEmail}
                    />

                    {/* Un peu dégueu :) */}
                    <br></br>

                    <TextField
                        error={checkConfirmEmail()}
                        required
                        id="outlined-password-input"
                        label="Confirmation adresse email"
                        helperText={checkConfirmEmail() ? "L'email doit contenir @" : ""}
                        value={emailConfirm}
                        onChange={handleEmailConfirm}
                    />

                    {/* Un peu dégueu :) */}
                    <br></br>


                    <TextField
                        error={checkPhone()}
                        required
                        id="outlined-password-input"
                        label="Numéro de téléphone"
                        helperText={checkPhone() ? "Il manque des numéros pour un numéro valide" : ""}
                        value={phone}
                        onChange={handlePhone}
                    />


                    <br></br>

                    <TextField
                        error={checkPassword()}
                        required
                        id="outlined-password-input"
                        label="Mot de passe"
                        helperText={checkPassword() ? "Le mot de passe doit faire au moins 8 caractères" : ""}
                        type="password"
                        value={password}
                        onChange={handlePassword}
                    />

                </div>

                <div>
                    <Button disabled={!validateForm()} variant="contained" onClick={handleSubmit}>
                        Créer mon compte
                    </Button>
                </div>

            </Box>

        </div>
    );
}
