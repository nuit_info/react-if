import * as React from "react";
import Head from 'next/head';
import { Box, Container, Grid } from '@mui/material';
import { Budget } from '../components/dashboard/budget';
import { LatestOrders } from '../components/dashboard/latest-orders';
import { LatestProducts } from '../components/dashboard/latest-products';
import { Sales } from '../components/dashboard/sales';
import { TasksProgress } from '../components/dashboard/tasks-progress';
import { TotalCustomers } from '../components/dashboard/total-customers';
import { TotalProfit } from '../components/dashboard/total-profit';
import { TrafficByDevice } from '../components/dashboard/traffic-by-device';
import { DashboardLayout } from '../components/dashboard-layout';
import Backdrop from '@mui/material/Backdrop';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import IconButton from '@mui/material/IconButton';
import TextField from '@mui/material/TextField';
import AddIcon from '@mui/icons-material/Add';
import MenuItem from '@mui/material/MenuItem';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import FormControl from '@mui/material/FormControl';
import Checkbox from '@mui/material/Checkbox';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';



const Dashboard = () => {

  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };


  const [category, setCategory] = React.useState('');
  const [open, setOpen] = React.useState(false);
  const [balance, setBalance] = React.useState(0);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [checked, setChecked] = React.useState(true);

  const handleChangeChecked = (event) => {
    setChecked(event.target.checked);
  };

  const handleBalanceChange = (event) => {
    setBalance(parseInt(event.target.value));
  }

  const handleSubmit = () => {
    console.log('bite');
  }

  const handleChange = (event) => {
    setCategory(event.target.value);
  };



  return (
    <>
      <Head>
        <title>
          Dashboard | Material Kit
        </title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8
        }}
      >
        <Container maxWidth={false}>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              xl={4}
              lg={4}
              sm={6}
              xs={12}
            >
              <TotalProfit sx={{ height: '100%' }} />
            </Grid>
            <Grid
              item
              lg={4}
              sm={4}
              xl={6}
              xs={12}
            >
              <Budget />
            </Grid>
            <Grid
              item
              xl={4}
              lg={4}
              sm={6}
              xs={12}
            >
              <TotalCustomers />
            </Grid>
            <Grid
              item
              lg={12}
              md={12}
              xl={12}
              xs={12}
            >
              <LatestOrders />
            </Grid>
          </Grid>
        </Container>
        <IconButton aria-label="add" className="addButton" color="secondary" onClick={handleOpen}>
          <AddIcon />
        </IconButton>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={open}>
            <Box sx={style}>
              <h2>Créer {balance > 0 ? "un crédit" : "une dépense"}</h2>
              <FormControl fullWidth>
                <TextField
                  variant="outlined"
                  required
                  margin="normal"
                  id="label-input"
                  label={balance > 0 ? "Label du crédit" : "Label de la dépense"} />
                <Select
                  labelId="select-label"
                  id="category-input"
                  margin="dense"
                  required
                  value={category}
                  label="Catégorie"
                  onChange={handleChange}
                >
                  <MenuItem value={1}>Loyer</MenuItem>
                  <MenuItem value={2}>Voiture</MenuItem>
                  <MenuItem value={3}>Courses</MenuItem>
                  <MenuItem value={4}>Loyer</MenuItem>
                </Select>
                <TextField
                  id="montant-input"
                  label="Montant"
                  margin="normal"
                  type="number"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={handleBalanceChange}
                />
                <FormGroup>
                  <FormControlLabel control={<Checkbox checked={checked} onChange={handleChangeChecked} />} label="Récurrent" />
                </FormGroup>
                {checked ?
                  <TextField
                    variant="outlined"
                    required
                    margin="normal"
                    id="recurrent-input"
                    label="Récurrence (tous les x jours)" />
                  : null}
                <br></br>
                <Button disabled={false} variant="contained" onClick={handleSubmit}>
                  Ajouter {balance > 0 ? "crédit" : "dépense"}
                </Button>
              </FormControl>

            </Box>
          </Fade>
        </Modal>

      </Box>
    </>
  )
}

Dashboard.getLayout = (page) => (
  <DashboardLayout>
    {page}
  </DashboardLayout>
);

export default Dashboard;
